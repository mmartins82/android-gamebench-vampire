package edu.brown.cs.systems.android.gamebench.hooks;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

/**
 * @author martins
 */
public class Setup implements IXposedHookLoadPackage {
    private static final String GAMEBENCH_PACKAGE = "com.gamebench" +
            ".metricscollector";

    @Override
    public void handleLoadPackage(LoadPackageParam lpparam) throws Throwable {
        if (lpparam.packageName.equals(GAMEBENCH_PACKAGE)) {
            EnvironmentHooks.getInstance().hook(lpparam);
            GpuHooks.getInstance().hook(lpparam);
            CpuHooks.getInstance().hook(lpparam);
            MemoryHooks.getInstance().hook(lpparam);
        }
    }
}
