package edu.brown.cs.systems.android.gamebench.hooks;

import android.os.Environment;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

import java.io.File;

import static de.robv.android.xposed.XposedHelpers.findAndHookMethod;
import static de.robv.android.xposed.XposedHelpers.getObjectField;
import static de.robv.android.xposed.XposedHelpers.setBooleanField;

/**
 * Capturer of events on GameBench's start
 * @author martins
 * @version 1.0
 * @since 2015-04-08
 */
public class EnvironmentHooks extends Hooks {
    public static final String TAG = Environment.class.getSimpleName();
    private static EnvironmentHooks mInstance;
    private File mBaseDir = null;

    private EnvironmentHooks() {
    }

    static EnvironmentHooks getInstance() {
        if (mInstance == null) {
            mInstance = new EnvironmentHooks();
        }

        return mInstance;
    }

    /**
     * Returns file path where GameBench data are stored
     * File pathname changes according to creation time
     * @return  filepath to GambeBench data
     */
    public String getBaseDirPath() {
        return (mBaseDir != null) ? mBaseDir.getAbsolutePath() : null;
    }

    @Override
    void hook(XC_LoadPackage.LoadPackageParam lpparam) {
        findAndHookMethod("com.gamebench.metricscollector.service" +
                        ".MetricsService", lpparam.classLoader, "init", new
                XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param)
                            throws Throwable {
                        super.beforeHookedMethod(param);
                        mBaseDir = (File) getObjectField(param.thisObject,
                                "mBaseDir");
                        // Enable collection of CPU core and freq switching
                        // NOTE: disabled by default
                        setBooleanField(param.thisObject,
                                "mCollectCpuStates", true);
                    }
                });
    }
}
