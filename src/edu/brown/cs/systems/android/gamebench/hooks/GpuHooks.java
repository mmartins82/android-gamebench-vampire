package edu.brown.cs.systems.android.gamebench.hooks;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;
import edu.brown.cs.systems.android.gamebench.utils.StatsUtils;

import java.io.FileWriter;
import java.util.List;

import static de.robv.android.xposed.XposedHelpers.findAndHookMethod;

/**
 * @author  martins
 * @version 1.0
 * @since   2015-04-04
 */
public class GpuHooks extends Hooks {
    public static final String TAG = GpuHooks.class.getSimpleName();
    private static GpuHooks mInstance;

    private GpuHooks() {
    }

    static GpuHooks getInstance() {
        if (mInstance == null) {
            mInstance = new GpuHooks();
        }

        return mInstance;
    }

    @Override
    public void hook(LoadPackageParam lpparam) {
        collectFps(lpparam);
        collectGpuUsage(lpparam);
        collectPvrMetrics(lpparam);
    }

    private void collectFps(LoadPackageParam lpparam) {
        findAndHookMethod("com.gamebench.metricscollector.threads" +
                ".FPSLoaderThread", lpparam.classLoader, "run", new
                XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param)
                            throws Throwable {
                        List<Integer> fpsValues = (List<Integer>) XposedHelpers
                                .getObjectField(param.thisObject, "sFpsValues");
                        List<Long> fpsTimestamps = (List<Long>) XposedHelpers
                                .getObjectField(param.thisObject,
                                        "sRelativeFpsTimeStamps");
                        int fpsMedian = (Integer) XposedHelpers.getObjectField
                                (param.thisObject, "sFPSmedian");
                        int fpsMAD = (Integer) XposedHelpers.getObjectField
                                (param.thisObject, "sFPSMedDev");

                        FileWriter fout = new FileWriter(SDCARD_PATH +
                                "/fpsSummary");

                        fout.write("FPS:");
                        for (int each : fpsValues) {
                            fout.write(String.format("%d,", each));
                        }

                        fout.write("\nTimestamps:");
                        for (long each : fpsTimestamps) {
                            fout.write(String.format("%d,", each));
                        }

                        fout.write(String.format("\nAvg:%.2f", StatsUtils
                                .getListAvg(fpsValues)));
                        fout.write(String.format("\nMedian:%d", fpsMedian));
                        fout.write(String.format("\nMAD:%d", fpsMAD));
                        fout.flush();
                        fout.close();
                    }
                });
    }

    private void collectGpuUsage(LoadPackageParam lpparam) {
        findAndHookMethod("com.gamebench.metricscollector.threads" +
                ".GPUUsageLoaderThread", lpparam.classLoader, "run", new
                XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param)
                            throws Throwable {
                        List<Float> usage = (List<Float>) XposedHelpers
                                .getObjectField(param.thisObject, "sGpuUsage");
                        List<Long> timestamps = (List<Long>) XposedHelpers
                                .getObjectField(param.thisObject,
                                        "sRelativeGpuUsageTimeStamps");
                        float median = (Float) XposedHelpers.getObjectField
                                (param.thisObject, "sGPUUsageMedian");

                        FileWriter fout = new FileWriter(SDCARD_PATH +
                                "/gpuUsage");

                        fout.write("GpuUsage:");
                        for (float each : usage) {
                            fout.write(String.format("%.2f,", each));
                        }

                        fout.write("\nTimestamps:");
                        for (long each : timestamps) {
                            fout.write(String.format("%d,", each));
                        }

                        fout.write(String.format("\nAvg:%.2f", StatsUtils
                                .getListAvg(usage)));
                        fout.write(String.format("\nMedian:%.2f", median));
                        fout.write(String.format("\nMAD:%.2f", StatsUtils
                                .getListMAD(usage)));
                        fout.flush();
                        fout.close();
                    }
                });
    }

    /**
     * Adds hook to collect and save specific metrics on ImgTech's PowerVR GPU.
     * Metrics include vertex and pixel load, as well as aggregate GPU load.
     * @param lpparam   Xposed's entry point to GameBench loading
     */
    private void collectPvrMetrics(LoadPackageParam lpparam) {
        findAndHookMethod("com.gamebench.metricscollector.threads" +
                ".ImgGPUUsageLoaderThread", lpparam.classLoader, "run", new
                XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws
                    Throwable {
                super.beforeHookedMethod(param);
                List<Float> pixelLoads = (List) XposedHelpers
                        .getObjectField(param.thisObject, "sFragments");
                List<Float> vertexLoads = (List) XposedHelpers
                        .getObjectField(param.thisObject, "sVertex");
                List<Long> timestamps = (List) XposedHelpers
                        .getObjectField(param.thisObject,
                                "sRelativeGpuUsageTimeStamps");
                List<Integer> numVertices = (List) XposedHelpers
                        .getObjectField(param.thisObject, "sNumVertices");
                float gpuUsageMedian = (Float) XposedHelpers.getObjectField
                        (param.thisObject, "sGPUUsageMedian");

                FileWriter fout = new FileWriter(SDCARD_PATH +
                        "/pvrMetrics");

                if (vertexLoads != null) {
                    fout.write("VertexLoad:");
                    for (float each : vertexLoads) {
                        fout.write(String.format("%.2f,", each));
                    }
                    fout.write(String.format("\nAvgVertexLoad:%.2f",
                            StatsUtils.getListAvg(vertexLoads)));
                    fout.write(String.format("\nMedianVertexLoad:%.2f",
                            StatsUtils.getListMedian(vertexLoads)));
                    fout.write(String.format("\nMADVertexLoad:%.2f",
                            StatsUtils.getListMAD(vertexLoads)));
                }

                if (numVertices != null) {
                    fout.write("\nNumVertices:");
                    for (int each : numVertices) {
                        fout.write(String.format("%d,", each));
                    }
                    fout.write(String.format("\nAvgVertices:%.2f", StatsUtils
                            .getListAvg(numVertices)));
                    fout.write(String.format("\nMedianVertices:%.2f", StatsUtils
                            .getListMedian(numVertices)));
                    fout.write(String.format("\nMADVertices:%.2f", StatsUtils
                            .getListMAD(numVertices)));
                }

                /*fout.write("\nNumFragments:");
                for (int each : numFragments) {
                    System.out.print(String.format("%d,", each));
                }
                fout.write(String.format("\nAvgFragments:%.2f", StatsUtils
                        .getListAvg(numFragments)));*/

                if (pixelLoads != null) {
                    fout.write("\nFragmentLoad:");
                    for (float each : pixelLoads) {
                        fout.write(String.format("%.2f,", each));
                    }
                    fout.write(String.format("\nAvgFragmentLoad:%.2f", StatsUtils
                            .getListAvg(pixelLoads)));
                    fout.write(String.format("\nMedianFragmentLoad:%.2f",
                            StatsUtils.getListMedian(pixelLoads)));
                    fout.write(String.format("\nMADFragmentLoad:%.2f",
                            StatsUtils.getListMAD(pixelLoads)));
                }

                if (timestamps != null) {
                    fout.write("\nTimestamps:");
                    for (long each : timestamps) {
                        fout.write(String.format("%d,", each));
                    }
                }

                fout.write(String.format("\nUsageMedian:%.2f", gpuUsageMedian));
                fout.flush();
                fout.close();
            }
        });
    }
}
