package edu.brown.cs.systems.android.gamebench.hooks;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;
import edu.brown.cs.systems.android.gamebench.utils.StatsUtils;

import java.io.FileWriter;
import java.util.List;

import static de.robv.android.xposed.XposedHelpers.findAndHookMethod;

/**
 * @author martins
 * @version 1.0
 * @since 2015-04-06
 */
public class MemoryHooks extends Hooks {
    public static final String TAG = GpuHooks.class.getSimpleName();
    private static MemoryHooks mInstance;

    private MemoryHooks() {
    }

    static MemoryHooks getInstance() {
        if (mInstance == null) {
            mInstance = new MemoryHooks();
        }

        return mInstance;
    }

    @Override
    void hook(XC_LoadPackage.LoadPackageParam lpparam) {
        collectMemUsage(lpparam);
    }

    /**
     * Adds hook to collect and save memory usage
     * @param lpparam   Xposed's entry point to GameBench loading
     */
    private void collectMemUsage(XC_LoadPackage.LoadPackageParam lpparam) {
        findAndHookMethod("com.gamebench.metricscollector.threads" +
                ".MemUsageLoaderThread", lpparam.classLoader, "run", new
                XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param)
                            throws Throwable {
                        List<Integer> totalUsage = (List<Integer>)
                                XposedHelpers.getObjectField(param
                                        .thisObject, "sMemValues");
                        List<Integer> nativeUsage = (List<Integer>)
                                XposedHelpers.getObjectField(param
                                        .thisObject, "sNativeMemValues");
                        List<Integer> dalvikUsage = (List<Integer>)
                                XposedHelpers.getObjectField(param
                                        .thisObject, "sDalvikMemValues");
                        List<Integer> otherUsage = (List<Integer>)
                                XposedHelpers.getObjectField(param
                                        .thisObject, "sOtherMemValues");
                        List<Long> timestamps = (List<Long>) XposedHelpers
                                .getObjectField(param.thisObject,
                                        "sMemRelativeTimeStamps");

                        FileWriter fout = new FileWriter(SDCARD_PATH +
                                "/memUsage");

                        fout.write("TotalUsage:");
                        for (int each : totalUsage) {
                            fout.write(String.format("%d,", each));
                        }
                        fout.write(String.format("\nTotalAvg:%.2f", StatsUtils
                                .getListAvg(totalUsage)));
                        fout.write(String.format("\nTotalMAD:%.2f", StatsUtils
                                .getListMAD(totalUsage)));

                        fout.write("\nNativeUsage:");
                        for (int each : nativeUsage) {
                            fout.write(String.format("%d,", each));
                        }
                        fout.write(String.format("\nNativeAvg:%.2f", StatsUtils
                                .getListAvg(nativeUsage)));
                        fout.write(String.format("\nNativeMAD:%.2f",
                                StatsUtils.getListMAD(nativeUsage)));

                        fout.write("\nDalvikUsage:");
                        for (int each : dalvikUsage) {
                            fout.write(String.format("%d,", each));
                        }
                        fout.write(String.format("\nDalvikAvg:%.2f", StatsUtils
                                .getListAvg(dalvikUsage)));
                        fout.write(String.format("\nDalvikMAD:%.2f", StatsUtils
                                .getListMAD(dalvikUsage)));

                        fout.write("\nOtherUsage:");
                        for (int each : otherUsage) {
                            fout.write(String.format("%d,", each));
                        }
                        fout.write(String.format("\nOtherAvg:%.2f", StatsUtils
                                .getListAvg(otherUsage)));
                        fout.write(String.format("\nOtherMAD:%.2f", StatsUtils
                                .getListMAD(otherUsage)));

                        fout.write("\nTimestamps:");
                        for(long each : timestamps) {
                            fout.write(String.format("%d,", each));
                        }

                        fout.flush();
                        fout.close();
                    }
                });
    }
}
