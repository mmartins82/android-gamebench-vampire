package edu.brown.cs.systems.android.gamebench.utils;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.List;

/**
 * @author martins
 * @version 1.0
 * @since 2015-04-08
 */
public class FileUtils {
    public static final String TAG = FileUtils.class.getSimpleName();

    /**
     * Check if file exists and is accessible
     *
     * @param filename  file name (with path)
     * @return          file exists or not
     */
    public static final boolean isFileAccessible(String filename) {
        File file = new File(filename);
        return (file.exists() && (file.length() > 0L));
    }

    /**
     * Returns collection of files matching name pattern
     *
     * @param path  path where files are located
     * @param expr  pattern-matching expression
     * @return      list of files matching expression
     */
    public static List<File> getFilesMatchingExpr(String path, final String
            expr) {
        File dir = new File(path);

        return Arrays.asList(dir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File file, String name) {
                return name.matches(expr);
            }
        }));
    }
}
