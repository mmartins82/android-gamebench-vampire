package edu.brown.cs.systems.android.gamebench.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author  martins
 * @version 1.0
 * @since   2015-04-05
 */
public class StatsUtils {
    public static final String TAG = StatsUtils.class.getSimpleName();

    /**
     * Returns average from number list
     * Assumption: input is list of numbers
     *
     * @param numset    list of numbers
     * @return          list average
     */
    public static double getListAvg(List numset) {
        double sum = 0.0;

        if ((numset == null) || (numset.size() == 0)) {
            return 0.0;
        }

        for (Object each : numset) {
            sum += ((Number) each).doubleValue();
        }

        return(sum / numset.size());
    }

    /**
     * Returns median from number list
     * Assumption: input is list of numbers
     * @param numset    list of numbers
     * @return          list median
     */
    public static double getListMedian(List<? extends Number> numset) {
        if ((numset == null) || (numset.size() == 0)) {
            return 0.0;
        }

        Number[] array = new Number[numset.size()];
        numset.toArray(array);

        Arrays.sort(array);
        return(array[numset.size() / 2].doubleValue());
    }

    /**
     * Returns MAD (Median Absolute Variation) from number list.
     * MAD is a robust version of standard deviation that indicates the
     * variability of the sample.
     * https://en.wikipedia.org/wiki/Median_absolute_deviation
     * Assumption: input is list of numbers
     * @param numset    list of numbers
     * @return          list MAD
     */
    public static double getListMAD(List<? extends Number> numset) {
        if ((numset == null) || (numset.size() == 0)) {
            return 0.0;
        }

        List<Double> array = new ArrayList<>();
        double med = StatsUtils.getListMedian(numset);

        for (int i = 0; i < numset.size(); i++) {
            array.add(Math.abs(numset.get(i).doubleValue() - med));
        }

        return getListMedian(array);
    }
}
