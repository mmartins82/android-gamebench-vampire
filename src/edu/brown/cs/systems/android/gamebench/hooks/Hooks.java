package edu.brown.cs.systems.android.gamebench.hooks;

import android.os.Environment;
import de.robv.android.xposed.XSharedPreferences;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

/**
 * @author martins
 */
public abstract class Hooks {
    public static final String TAG = Hooks.class.getSimpleName();
    public static final String PACKAGE_NAME = "edu.brown.cs.systems.android" +
            ".gamebench";
    public static final String SDCARD_PATH = Environment
            .getExternalStorageDirectory().getPath() + "/bench-data";
    XSharedPreferences mPreferences = new XSharedPreferences(PACKAGE_NAME,
            "global");

    abstract void hook(XC_LoadPackage.LoadPackageParam lpparam);

    public void debugLog(String log) {
        String curLevel = mPreferences.getString("logging_level",
                "default");
        if (curLevel.equals("verbose")) {
            XposedBridge.log(TAG + ": " + log);
        }
    }

    public void defaultLog(String log) {
        String curLevel = mPreferences.getString("logging_level",
                "default");

        if (curLevel.equals("default") || curLevel.equals("verbose")) {
            XposedBridge.log(TAG + ": " + log);
        }
    }
}
