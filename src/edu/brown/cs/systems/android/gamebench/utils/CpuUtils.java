package edu.brown.cs.systems.android.gamebench.utils;

import java.io.File;
import java.util.List;

/**
 * @author martins
 * @version 1.0
 * @since 2015-04-08
 */
public class CpuUtils {
    private static final String TAG = CpuUtils.class.getSimpleName();
    private static final String SYSFS_CPU_PATH = "/sys/devices/system/cpu";

    /**
     * Returns number of CPU cores in device
     * @return  number of CPU cores
     */
    public static int getNumCores() {
        List<File> files = FileUtils.getFilesMatchingExpr(SYSFS_CPU_PATH,
                "cpu\\d+");

        return (files != null) ? files.size() : 0;
    }
}
