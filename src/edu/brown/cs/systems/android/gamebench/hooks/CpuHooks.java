package edu.brown.cs.systems.android.gamebench.hooks;

import com.gamebench.metricscollector.protobuf.CpuFreqResidencyPbMessage.CpuFreqResidencyMessage;
import com.gamebench.metricscollector.protobuf.CpuStatePbMessage.CpuStateMessage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;
import edu.brown.cs.systems.android.gamebench.utils.CpuUtils;
import edu.brown.cs.systems.android.gamebench.utils.FileUtils;
import edu.brown.cs.systems.android.gamebench.utils.StatsUtils;

import static de.robv.android.xposed.XposedHelpers.findAndHookMethod;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author  martins
 * @version 1.1
 * @since   2015-04-06
 */
public class CpuHooks extends Hooks {
    public static final String TAG = GpuHooks.class.getSimpleName();
    private static CpuHooks mInstance;

    private CpuHooks() {
    }

    static CpuHooks getInstance() {
        if (mInstance == null) {
            mInstance = new CpuHooks();
        }

        return mInstance;
    }

    @Override
    public void hook(LoadPackageParam lpparam) {
        collectUsage(lpparam);
    }

    private void collectFrequencyResidency(LoadPackageParam lpparam) {
        findAndHookMethod("com.gamebench.metricscollector.threads" +
                        ".CPUStatesLoaderThread", lpparam.classLoader, "run",
                new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param)
                            throws Throwable {
                        super.afterHookedMethod(param);
                        List<Long> freqs = (List<Long>) XposedHelpers
                                .getObjectField(param.thisObject, "sFreqs");
                        List<Float> freqRates = (List<Float>) XposedHelpers
                                .getObjectField(param.thisObject,
                                        "sCPUFreqPct");
                        List<Long> freqDurations = (List<Long>) XposedHelpers
                                .getObjectField(param.thisObject,
                                        "sDurations");

                        FileWriter fout = new FileWriter(SDCARD_PATH +
                                "/cpuFreqResidency");

                        fout.write("Freqs:");
                        for (long each : freqs) {
                            fout.write(String.format("%d,", each));
                        }

                        fout.write("\nDurations:");
                        for (long each : freqDurations) {
                            fout.write(String.format("%d,", each));
                        }

                        fout.write("\nResidencyRate:");
                        for (float each : freqRates) {
                            fout.write(String.format("%f,", each));
                        }

                        fout.flush();
                        fout.close();
                    }
                });
    }

    private void collectFrequencyResidencyAlternate(String filename) {
        List<Float> frequencies = new ArrayList<>();
        List<Long> durations = new ArrayList<>();
        long totalTime = 1; // avoid division by zero

        try {
            DataInputStream din = new DataInputStream(new FileInputStream
                    (filename));
            while (din.available() != 0) {
                CpuFreqResidencyMessage message = CpuFreqResidencyMessage
                        .parseDelimitedFrom(din);
                durations = message.getDurationsList();
                frequencies = message.getFrequenciesList();
                totalTime = message.getTotalTime();
            }

            FileWriter fout = new FileWriter(SDCARD_PATH + "/cpuFreqResidency");

            fout.write("Freqs:");
            for (float each : frequencies) {
                 fout.write(String.format("%d,", (long) each));
            }

            fout.write("\nDurations:");
            for (long each : durations) {
                fout.write(String.format("%d,", each));
            }

            fout.write("\nResidencyRate:");
            for (long each : durations) {
                fout.write(String.format("%.2f,", each / (totalTime * 1.0f)));
            }

            fout.flush();
            fout.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void collectCoreStates(LoadPackageParam lpparam) {
        findAndHookMethod("com.gamebench.metricscollector.threads" +
                ".CPUFrequencyLoaderThread", lpparam.classLoader, "run", new
                XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param)
                            throws Throwable {
                        super.afterHookedMethod(param);
                        int numCores = (Integer) XposedHelpers.getObjectField
                                (param.thisObject, "numCores");
                        List<Long> switchTimestamps = (List<Long>)
                                XposedHelpers.getObjectField(param
                                                .thisObject,
                                        "sRelativeCpuFrequencyTimestamps");
                        List<List<Float>> freqs = (List<List<Float>>)
                                XposedHelpers.getObjectField(param.thisObject,
                                        "sCpuFrequency");
                        List<List<Long>> cpuOn = (List<List<Long>>)
                                XposedHelpers.getObjectField(param
                                        .thisObject, "sCpuOn");

                        FileWriter fout = new FileWriter(SDCARD_PATH +
                                "/cpuStates");

                        for (int i = 0; i < numCores; i++) {
                            fout.write(String.format("\nCore%d_Freqs:", i));
                            for (float each : freqs.get(i)) {
                                fout.write(String.format("%d,", (long) each));
                            }

                            fout.write(String.format("\nCore%d_Online:", i));
                            for (long each : cpuOn.get(i)) {
                                fout.write(String.format("%d,", each));
                            }
                        }

                        fout.write("\nTimestamps:");
                        for (long each : switchTimestamps) {
                            fout.write(String.format("%d,", each));
                        }

                        fout.flush();
                        fout.close();
                    }
                });
    }

    public void collectCoreStatesAlternate(String filename) {
        List<List<Float>> frequencies = new ArrayList<>();
        List<List<Boolean>> coreOn = new ArrayList<>();
        List<Long> timestamps = new ArrayList<>();

        int numCores = CpuUtils.getNumCores();

        for (int i = 0; i < numCores; i++) {
            frequencies.add(new ArrayList<Float>());
            coreOn.add(new ArrayList<Boolean>());
        }

        // Early return
        if (!FileUtils.isFileAccessible(filename)) {
            return;
        }

        long baseTstamp = -1;
        try {
            DataInputStream din = new DataInputStream(new FileInputStream
                    (filename));
            while (din.available() != 0) {
                CpuStateMessage message = CpuStateMessage.parseDelimitedFrom
                        (din);
                List<Float> freqs = message.getCoreFrequencyList();
                List<Boolean> ons = message.getCoreOnList();

                long tstamp = message.getTimestamp();
                if (baseTstamp == -1) {
                    baseTstamp = tstamp;
                }

                timestamps.add(tstamp - baseTstamp);

                for (int i = 0; i < numCores; i++) {
                    frequencies.get(i).add(freqs.get(i));
                    coreOn.get(i).add(ons.get(i));
                }
            }

            din.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileWriter fout = new FileWriter(SDCARD_PATH + "/cpuStates");

            for (int i = 0; i < numCores; i++) {
                fout.write(String.format("\nCore%d_Freqs:", i));
                for (float each : frequencies.get(i)) {
                    fout.write(String.format("%d,", (long) each));
                }

                fout.write(String.format("\nCore%d_Online:", i));
                for (boolean each : coreOn.get(i)) {
                    fout.write(String.format("%d,", each ? 1 : 0));
                }
            }

            fout.write("\nTimestamps:");
            for (long each : timestamps) {
                fout.write(String.format("%d,", each));
            }

            fout.flush();
            fout.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void collectUsage(LoadPackageParam lpparam) {
        findAndHookMethod("com.gamebench.metricscollector.threads" +
                ".CPUUsageLoaderThread", lpparam.classLoader, "run", new
                XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param)
                            throws Throwable {
                        super.afterHookedMethod(param);
                        Object cpuData = XposedHelpers.getObjectField(param
                                .thisObject, "cpuData");
                        List<List<Float>> coresUsage = (List<List<Float>>)
                                XposedHelpers.callMethod(cpuData,
                                        "getCoresUsage");
                        List<Float> totalUsage = (List<Float>) XposedHelpers
                                .callMethod(cpuData, "getTotalCpuUsage");
                        List<Long> usageStamps = (List<Long>) XposedHelpers
                                .callMethod(cpuData,
                                        "getRelativeCpuUsageTimeStamps");
                        List<Float> usageMetrics = (List<Float>)
                                XposedHelpers.callMethod(cpuData,
                                        "getCpuUsageMetrics");
                        List<Float> usageDaemon = (List<Float>) XposedHelpers
                                .callMethod(cpuData, "getCpuUsageDaemon");
                        List threadNames = (List) XposedHelpers.callMethod
                                (cpuData, "getThreadsName");
                        List threadUsages = (List) XposedHelpers.callMethod
                                (cpuData, "getThreadsUsage");

                        FileWriter fout = new FileWriter(SDCARD_PATH +
                                "/cpuUsage");
                        for (int i = 0; i < coresUsage.size(); i++) {
                            fout.write(String.format("\nCore%dUsage:", i));
                            for (float each : coresUsage.get(i)) {
                                fout.write(String.format("%.2f,", each));
                            }
                        }

                        fout.write("\nCpuUsage:");
                        for (float each : totalUsage) {
                            fout.write(String.format("%.2f,", each));
                        }
                        fout.write(String.format("\nCpuUsageAvg:%.2f",
                                StatsUtils.getListAvg(totalUsage)));
                        fout.write(String.format("\nCpuUsageMedian:%.2f",
                                StatsUtils.getListMedian(totalUsage)));
                        fout.write(String.format("\nCpuUsageMAD:%.2f",
                                StatsUtils.getListMAD(totalUsage)));

                        fout.write("\nUsageTimestamps:");
                        for (long each : usageStamps) {
                            fout.write(String.format("%d,", each));
                        }

                        fout.write("\nUsageMetrics:");
                        for (float each : usageMetrics) {
                            fout.write(String.format("%.2f,", each));
                        }
                        fout.write(String.format("\nUsageMetricsAvg:%.2f",
                                StatsUtils.getListAvg(usageMetrics)));
                        fout.write(String.format("\nUsageMetricsMedian:%.2f",
                                StatsUtils.getListMedian(usageMetrics)));
                        fout.write(String.format("\nUsageMetricsMAD:%.2f",
                                StatsUtils.getListMAD(usageMetrics)));

                        fout.write("\nUsageDaemon:");
                        for (float each : usageDaemon) {
                            fout.write(String.format("%.2f,", each));
                        }
                        fout.write(String.format("\nUsageDaemonAvg:%.2f",
                                StatsUtils.getListAvg(usageDaemon)));
                        fout.write(String.format("\nUsageDaemonMedian:%.2f",
                                StatsUtils.getListMedian(usageDaemon)));
                        fout.write(String.format("\nUsageDaemonMAD:%.2f",
                                StatsUtils.getListMAD(usageDaemon)));

                        fout.flush();
                        fout.close();

                        String dir = EnvironmentHooks.getInstance()
                                .getBaseDirPath();
                        collectCoreStatesAlternate(dir +
                                "/CPUFrequencyMessage");
                        collectFrequencyResidencyAlternate(dir +
                                "/CPUStatesMessage");
                    }
                });
    }
}
